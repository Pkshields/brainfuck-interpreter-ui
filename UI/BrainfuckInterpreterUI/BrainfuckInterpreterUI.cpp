#include "BrainfuckInterpreterUI.h"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <exception>
#include <QFileDialog>
#include "Interpreter.h"

#ifdef _WIN32
	#define popen _popen
	#define pclose _pclose
#endif

using namespace std;

BrainfuckInterpreterUI::BrainfuckInterpreterUI(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	// UI Setup
	this->setFixedSize(this->size());

	// Event setup
	connect(ui.interpretButton, SIGNAL(released()), this, SLOT(handleInterpretButton()));
	connect(ui.resultEdit, SIGNAL(textChanged()), this, SLOT(handleResultEditInput()));
	connect(ui.importFileButton, SIGNAL(released()), this, SLOT(handleImportFileButton()));
}

BrainfuckInterpreterUI::~BrainfuckInterpreterUI()
{

}

void BrainfuckInterpreterUI::handleInterpretButton()
{
	// Sets up the coide to be run & starts running it
	// after code is run, cleanup is handled later

	QString code;
	char inputChar;

	if (config != NULL || interpreter != NULL)
	{
		return;
	}

	ui.resultEdit->clear();
	code = ui.codeEdit->toPlainText();

	try
	{
		string test1 = code.toStdString();
		const char* test2 = test1.c_str();

		const char* encapsulateCodeIntoTerminalArgv[1] = { test2 };
		config = new Config(1, encapsulateCodeIntoTerminalArgv);
		interpreter = new Interpreter(config);

		runCode();
	}
	catch (exception& e) {
		// The most beautiful of code
		string exception = "EXCEPTION: " + string(e.what());
		ui.resultEdit->insertPlainText(QString(("\n" + exception).c_str()));
	}
}

void BrainfuckInterpreterUI::runCode()
{
	BrainfuckIO result;

	do
	{
		result = interpreter->interpret();

		if (result == OUTPUT)
		{
			ui.resultEdit->moveCursor(QTextCursor::End);
			ui.resultEdit->insertPlainText(QString(interpreter->getOutput()));
			ui.resultEdit->moveCursor(QTextCursor::End);
		}
		else if (result == INPUT)
		{
			ui.resultEdit->setReadOnly(false);
			ui.statusBar->showMessage("Waiting for input - please input a character into the result text box");
			return;
		}
	} while (result != FINISHED);

	delete interpreter;
	interpreter = NULL;
	delete config;
	config = NULL;
}

void BrainfuckInterpreterUI::handleResultEditInput()
{
	QString resultExitText = ui.resultEdit->toPlainText();
	if (!ui.resultEdit->isReadOnly() && resultExitText.length() > 0)
	{
		QChar qInputChar = resultExitText[resultExitText.length() - 1];
		char inputChar = qInputChar.toLatin1();

		interpreter->setInput(inputChar);

		ui.resultEdit->setReadOnly(true);
		ui.statusBar->clearMessage();
		runCode();
	}
}

void BrainfuckInterpreterUI::handleImportFileButton()
{
	QString path = QFileDialog::getOpenFileName(this, tr("Directory"));
	if (path.isNull() == false)
	{
		string line;
		ifstream fileImport;

		ui.codeEdit->clear();

		fileImport.open(path.toStdString());

		if (!fileImport.is_open())
		{
			ui.codeEdit->moveCursor(QTextCursor::End);
			ui.codeEdit->insertPlainText("Error while opening file");
		}

		while (getline(fileImport, line)) {
			ui.codeEdit->appendPlainText(QString(line.c_str()));
		}

		if (fileImport.bad())
		{
			ui.codeEdit->moveCursor(QTextCursor::End);
			ui.codeEdit->insertPlainText("Error while reading file");
		}

		fileImport.close();
	}
}