#ifndef BRAINFUCKINTERPRETERUI_H
#define BRAINFUCKINTERPRETERUI_H

#include <QtWidgets/QMainWindow>
#include "ui_BrainfuckInterpreterUI.h"

#include "Config.h"
#include "Interpreter.h"

class BrainfuckInterpreterUI : public QMainWindow
{
	Q_OBJECT

public:
	BrainfuckInterpreterUI(QWidget *parent = 0);
	~BrainfuckInterpreterUI();

private slots:
	void handleInterpretButton();
	void handleResultEditInput();
	void handleImportFileButton();

private:
	void runCode();

private:
	Ui::BrainfuckInterpreterUIClass ui;

	Config* config = NULL;
	Interpreter* interpreter = NULL;
};

#endif // BRAINFUCKINTERPRETERUI_H
