#include "Data.h"

#include "BFException.h"

Data::Data()
{
	for (int i = 0; i < arrayLength; i++) {
		dataArray[i] = 0;
	}
}

Data::~Data()
{
}

void Data::nextCell()
{
	if (currentPosition < &dataArray[arrayLength-1])
	{
		++currentPosition;
	}
	else
	{
		throw BFException("Data already at last position; could not move forwards.");
	}
}

void Data::previousCell()
{
	if (currentPosition > &dataArray[0])
	{
		--currentPosition;
	}
	else
	{
		throw BFException("Data already at first position; could not move backwards.");
	}
}

void Data::incrementData()
{
	++*currentPosition;
}

void Data::decrementData()
{
	--*currentPosition;
}

char Data::getData()
{
	return *currentPosition;
}

void Data::setData(char data)
{
	*currentPosition = data;
}
