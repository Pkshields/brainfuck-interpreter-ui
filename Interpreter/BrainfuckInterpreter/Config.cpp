#include "Config.h"

#ifndef _WIN32
	#include <string.h>
#endif

using namespace std;

Config::Config(int argc, const char* argv[])
{
	const char* currentArg;

	for (int i = 0; i < argc; ++i)
	{
		currentArg = argv[i];

		if (strcmp(currentArg, "-f") == 0)
		{
			codeData = string(argv[++i]);
			codeDataType = FILELOC;
		}
		else
		{
			codeData = string(currentArg);
			codeDataType = STRAIGHT;
		}
	}
}

Config::~Config()
{
}

CodeType Config::getCodeDataType()
{
	return codeDataType;
}

std::string* Config::getCodeData()
{
	return &codeData;
}

std::string* Config::getFileLoc()
{
	return getCodeData();
}

std::string* Config::getRawCode()
{
	return getCodeData();
}
