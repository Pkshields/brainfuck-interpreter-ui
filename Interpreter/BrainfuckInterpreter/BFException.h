#pragma once

#include <exception>
#include <string>

class BFException : public std::exception
{
public:
	BFException(std::string exceptionString) : exString(exceptionString) {};
	~BFException() {};

	virtual const char* what() const throw()
	{
		return exString.c_str();
	}

private:
	std::string exString;
};

