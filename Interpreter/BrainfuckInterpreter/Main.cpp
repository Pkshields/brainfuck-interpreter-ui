#include <stdio.h>
#include <string>
#include <iostream>
#include "Interpreter.h"

using namespace std;

#ifdef _WIN32
	#include <conio.h>
	#define getch _getch
#else
	#include <curses.h>
#endif

Config* config;
Interpreter* interpreter;

int main(int argc, const char* argv[])
{
	BrainfuckIO result;
	char inputChar;

	// Curses specific code to get getch to work
#ifndef _WIN32
	initscr();
	timeout(-1);
#endif

	try
	{
		config = new Config(argc, argv);
		interpreter = new Interpreter(config);

		do
		{
			result = interpreter->interpret();

			if (result == OUTPUT)
			{
				cout << interpreter->getOutput();
			}
			else if (result == INPUT)
			{
				inputChar = getch();
				cout << inputChar;
				interpreter->setInput(inputChar);
			}
		} while (result != FINISHED);
	}
	catch (exception& e) {
		cout << endl << "EXCEPTION: " << e.what();
	}

	delete interpreter;
	delete config;
	
#ifndef _WIN32	
	endwin();
	cout << endl;
#endif

}