#pragma once
class Data
{
public:
	Data();
	~Data();

	void nextCell();
	void previousCell();
	void incrementData();
	void decrementData();
	char getData();
	void setData(char);

private:
	static const int arrayLength = 30000;
	char dataArray[arrayLength];
	char *currentPosition = dataArray;
};

