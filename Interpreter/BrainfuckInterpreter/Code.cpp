#include "Code.h"

#include <fstream>
#include <iostream>
#include "BFException.h"

#ifndef _WIN32
	#include <algorithm>
#endif

using namespace std;

//Stupid C++ static array initialization bullshit
const char Code::validChar[] = { '+', '-', '>', '<', '.', ',', '[', ']' };
 
Code::Code(string* code, CodeType type)
{
	switch (type) {
	case STRAIGHT: 
		this->code = *code;
		break;
	case FILELOC:
		readFromFile(code);
		break;
	}

	codeLength = this->code.length();
}

Code::~Code()
{
}

char Code::nextChar()
{
	char nextChar;

	while (true)
	{
		if (hasNext())
		{
			nextChar = code.at(currentChar++);

			if (find(validChar, validChar + validCharLength, nextChar) != validChar + validCharLength)
			{
				return nextChar;
			}
			else
			{
				// Duplicate hasNext check so that the exception can be thrown if the nextChar call is user error,
				// but return dummy data if it is due to comments
				if (!hasNext())
				{
					return ' ';
				}
			}
		}
		else
		{
			throw BFException("End of code, no more characters to read.");
		}
	}
}

bool Code::hasNext()
{
	return currentChar < codeLength;
}

unsigned int Code::getPosition()
{
	return currentChar-1;
}

void Code::setPosition(unsigned int position)
{
	currentChar = position;
}

void Code::readFromFile(std::string* fileLoc)
{
	ifstream codeFile;
	string line;

	codeFile.open(*fileLoc);
	if (codeFile.is_open())
	{
		while (getline(codeFile, line))
		{
			code += line;
		}
		codeFile.close();
	}
	else
	{
		throw BFException("Could not read from file: " + *fileLoc);
	}
}