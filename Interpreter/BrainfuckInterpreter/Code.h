#pragma once

#include <string>
#include "Config.h"

class Code
{
public:
	Code(std::string*, CodeType);
	~Code();

	char nextChar();
	bool hasNext();
	unsigned int getPosition();
	void setPosition(unsigned int);

private:
	static const int validCharLength = 8;
	static const char validChar[validCharLength];

	std::string code;
	unsigned int codeLength;
	unsigned int currentChar = 0;

	void readFromFile(std::string* fileLoc);
};

