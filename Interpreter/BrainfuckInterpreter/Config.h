#pragma once

#include <string>

enum CodeType { NONE, STRAIGHT, FILELOC };

class Config
{
public:
	Config(int, const char*[]);
	~Config();

	CodeType getCodeDataType();
	std::string* getCodeData();
	std::string* getFileLoc();
	std::string* getRawCode();

private:
	CodeType codeDataType = NONE;
	std::string codeData;
};

