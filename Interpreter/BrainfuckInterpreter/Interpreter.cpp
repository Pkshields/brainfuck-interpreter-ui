#include "Interpreter.h"

#include "BFException.h"

Interpreter::Interpreter(Config* config)
{
	this->config = config;
	this->code = new Code(config->getCodeData(), config->getCodeDataType());
}

Interpreter::~Interpreter()
{
}

BrainfuckIO Interpreter::interpret()
{
	char currentChar;

	// Check if IO is required first - defined by the Brainfuck language
	if (outputChar != 0)
	{
		throw BFException("Need to read Output before continuing to execute code.");
	}
	else if (waitForInput)
	{
		throw BFException("Need to read Input before continuing to execute code.");
	}

	// Execute code
	while (code->hasNext())
	{
		currentChar = code->nextChar();

		switch (currentChar)
		{
		case '+':
			data.incrementData();
			break;

		case '-':
			data.decrementData();
			break;

		case '>':
			data.nextCell();
			break;

		case '<':
			data.previousCell();
			break;

		case '.':
			outputChar = data.getData();
			return OUTPUT;

		case ',':
			waitForInput = true;
			return INPUT;

		case '[':
			if (data.getData() == 0)
			{
				skipIf();
			}
			else
			{
				startingBraceStack.push(code->getPosition());
			}
			break;

		case ']':
			if (data.getData() != 0)
			{
				restartWhile();
			}
			else
			{
				startingBraceStack.pop();
			}
			break;
		}
	}

	return FINISHED;
}

void Interpreter::skipIf()
{
	char currentChar;
	int numNestedIfs = 0;

	while (code->hasNext())
	{
		currentChar = code->nextChar();

		if (currentChar == '[')
		{
			numNestedIfs++;
		}
		else if (currentChar == ']')
		{
			if (numNestedIfs == 0)
			{
				break;
			}
			else
			{
				numNestedIfs--;
			}
		}
	}
}

void Interpreter::restartWhile()
{
	unsigned int bracePosition = startingBraceStack.top();
	startingBraceStack.pop();
	code->setPosition(bracePosition);
}


char Interpreter::getOutput()
{
	char temp = outputChar;
	outputChar = 0;
	return temp;
}

void Interpreter::setInput(char input)
{
	data.setData(input);
	waitForInput = false;
}