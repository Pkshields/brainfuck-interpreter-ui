#pragma once

#include "Data.h"
#include "Code.h"
#include <stack>

enum BrainfuckIO { OUTPUT, INPUT, FINISHED };

class Interpreter
{
public:
	Interpreter(Config*);
	~Interpreter();

	BrainfuckIO interpret();

	char getOutput();
	void setInput(char);

private:
	void skipIf();
	void restartWhile();

private:
	Data data;
	Code *code;
	Config *config;
	std::stack <unsigned int> startingBraceStack;

	char outputChar = 0;
	bool waitForInput = false;
};