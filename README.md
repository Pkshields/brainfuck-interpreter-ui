# Brainfuck Interpreter & UI

"Brainfuck Interpreter & UI" is my own attempt at creating a brainfuck interpreter and UI in cross platform C++ and Qt.

Interpreter Usage
-----------------

Simply feed the executable some Brainfuck code in quotes and it will interpret:

> Brainfuck.exe ",>++++ [>++++++++<-] >.....<<."

Or, you can feed it a text file containing Brainfuck code, using the -f flag

> Brainfuck.exe -f BFCode.txt

License
-------

"Brainfuck Interpreter & UI" is licensed under the New BSD License.